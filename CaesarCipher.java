//************************************************************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Luke Regan
// Ayodele Hamilton
// CMPSC 111 Fall 2014
// Final Project
// Date: December 4, 2014
// Purpose: The purpose of the project is to encrypt and decrypt secret messages.
//************************************************************************************
import java.util.Date;
import java.util.Scanner;
import javax.crypto.Cipher;

public class CaesarCipher
{
  // Simple main method for testing the Caesar cipher
  public static void main(String[] args)
  {
     Scanner scan = new Scanner(System.in);

    scan.useDelimiter("\\n");
    System.out.println("Welcome to the Caesar Cipher encrypter and decrypter!");
    int i = -1;

    while(i != 0)
    {
        System.out.println("Enter '1' to encrypt, '2' to decrypt or '0' to quit.");
    	i = scan.nextInt();

    	switch (i)

    	{
    		case 1:
    			System.out.println("What is the message you would like to encrypt?");
    			String secret = scan.next();
    			secret = secret.toUpperCase();
                Encryption cipher = new Encryption();
                secret = cipher.encrypt(secret);
    			System.out.println(secret);             // the ciphertext
    			break;

    		case 2:
    			System.out.println("What is the message you would like to decrypt?");
    			String secret2 = scan.next();
    			secret2 = secret2.toUpperCase();
                Decryption caesar =  new Decryption();
    			secret2 = caesar.decrypt(secret2);
    			System.out.println(secret2);            // should be plaintext again
    			break;

            case 3:
                System.out.println("Would you like to Email it?");


    		case 0:
    			System.out.println("Thank you for using the Caesar Cipher encrypter and decrypter!");
    			System.exit(0);
    			break;
        }
    }
  }
}
