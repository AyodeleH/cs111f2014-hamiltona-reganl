//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton
//Luke Regan
//CMPSC 111 Fall 2014
// Final Project
//Date: 12/7/14
//
//Purpose:To encrypt and decrypt code using the Caesar method.
//***************************************
//import java.util.io;
import java.util.Scanner;
import java.util.Date;
import javax.crypto.Cipher;

public class Decryption
{

  public static final int ALPHASIZE = 26; // English alphabet (uppercase only)
  public static final char[] alpha = {'A','B','C','D','E','F','G','H', 'I',
    'J','K','L','M', 'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
  protected char[] encrypt = new char[ALPHASIZE];  // Encryption array
  protected char[] decrypt = new char[ALPHASIZE];  // Decryption array

 public Decryption()
  {
    for (int i=0; i<ALPHASIZE; i++){
      encrypt[i] = alpha[(i + 3) % ALPHASIZE];
      decrypt[encrypt[i] - 'A'] = alpha[i];
    }
    return;
  }

 // Decryption method
  public String decrypt(String secret2)
  {
    char[] message = secret2.toCharArray();     // the message array
    for (int i=0; i<message.length; i++){       // decryption loop
      if (Character.isUpperCase(message[i]))   // we have a letter to change
        message[i] = decrypt[message[i] - 'A'];   // use letter as an index
    }
    return new String(message);
  }
 }

