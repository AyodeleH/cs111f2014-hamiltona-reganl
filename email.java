package javax.mail.jar;

import java.util.Properties;
import java.util.Scanner;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class email
{
	public static void main(String[] args)
	{
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		Scanner scan = new Scanner(System.in);

		System.out.print("Username for Authentication : ");
		final String username = scan.nextLine();

		System.out.print("Password for Authentication : ");
		final String password = scan.nextLine();

		System.out.print("From email : ");
		final String fromEmailAddress = scan.nextLine();

		System.out.print("To email : ");
		final String toEmailAddress = scan.nextLine();

		System.out.print("Subject : ");
		final String subject = scan.nextLine();

		System.out.print("Message : ");
		final String textMessage = scan.nextLine();


		Session session = Session.getDefaultInstance(properties, new Authenticator(){
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmailAddress));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmailAddress));
			message.setSubject(subject);
			message.setText(textMessage);
			Transport.send(message);

			System.out.println("Your Message delivered successfully ....");
			}
		catch (MessagingException e)
		{
			throw new RuntimeException(e);
		}
	}
}
